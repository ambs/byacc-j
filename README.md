# byacc-j

This is a clone of BYACC/J as available in http://byaccj.sourceforge.net/ 

Berkeley Yacc and/or Byacc-j is distributed with no warranty whatever.  
The author and any other contributors take no responsibility for the
consequences of its use.
